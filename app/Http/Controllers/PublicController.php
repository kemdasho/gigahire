<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

class PublicController extends Controller
{
    public function publicPageLanding()
    {
    	return view('template/page/landing');
    }

    public function publicPageSingleEvent($event)
    {
    	return view('template/page/singleEvent');
    }

    public function publicActionBookEvent(Request $request)
    {
    	// validate user input params
        $validator = Validator::make($request->only('fullname', 'email', 'mobile'), [
            'fullname' => 'required',
            'email' => 'required',
            'mobile' => 'required'
        ]);

        // Return back with errors
        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

		if(true) {
            return redirect()->route('publicPageBookSuccess');
        }

    }

    public function publicActionLeaveRating(Request $request)
    {
    	# code...
    }

    public function publicPageBookSuccess()
    {
    	$bodyHeader = [
    		'name' => 'Booking'
    	];

    	return view('template/page/bookSuccess', [
    		'bodyHeader' => $bodyHeader,
    	]);
    }

    public function publicPageCheckout()
    {
    	$bodyHeader = [
    		'name' => 'Booking'
    	];

    	return view('template/page/bookCheckout', [
    		'bodyHeader' => $bodyHeader,
    	]);
    }

    public function publicPageCheckoutSuccess()
    {
    	$bodyHeader = [
    		'name' => 'Booking'
    	];

    	return view('template/page/checkoutSuccess', [
    		'bodyHeader' => $bodyHeader,
    	]);
    }

    public function publicPageBookManager()
    {
    	$bodyHeader = [
    		'name' => 'Booking'
    	];

    	return view('template/page/bookingManager', [
    		'bodyHeader' => $bodyHeader,
    	]);
    }

    public function publicPageBookInvoice()
    {
    	return view('template/page/invoice');
    }

    public function publicPageTeam()
    {
        $bodyHeader = [
            'name' => 'About Us'
        ];

        return view('template/page/team', [
            'bodyHeader' => $bodyHeader,
        ]);
    }

    public function publicPageReview()
    {
        $bodyHeader = [
            'name' => 'Review'
        ];

        return view('template/page/review', [
            'bodyHeader' => $bodyHeader,
        ]);
    }

    public function publicPagePricing()
    {
        $bodyHeader = [
            'name' => 'Review'
        ];

        return view('template/page/pricing', [
            'bodyHeader' => $bodyHeader,
        ]);
    }

    public function publicPageContact()
    {
        $bodyHeader = [
            'name' => 'Review'
        ];

        return view('template/page/contact', [
            'bodyHeader' => $bodyHeader,
        ]);
    }

    public function publicPageBlog()
    {
        $bodyHeader = [
            'name' => 'Review'
        ];

        return view('template/page/blog', [
            'bodyHeader' => $bodyHeader,
        ]);
    }

    public function publicPageSingleBlog($blog)
    {
        $bodyHeader = [
            'name' => 'Review'
        ];

        return view('template/page/singleBlog', [
            'bodyHeader' => $bodyHeader,
        ]);
    }
}
