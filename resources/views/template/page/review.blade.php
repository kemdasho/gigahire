@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')



		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>What Our Clients Say</h2>
					<p>GigaHire makes it easy for you by keeping it simple. We offer one base package that includes all of the essentials for a great celebration!</p>
				</div>
				<section>

				<div class="reviews-container add_bottom_30">
					<div class="row">
						<div class="col-lg-3">
							<div id="review_summary">
								<strong>8.5</strong>
								<em>Superb</em>
								<small>Based on 4 reviews</small>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="row">
								<div class="col-lg-10 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-2 col-3"><small><strong>5 stars</strong></small></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-10 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-2 col-3"><small><strong>4 stars</strong></small></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-10 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-2 col-3"><small><strong>3 stars</strong></small></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-10 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-2 col-3"><small><strong>2 stars</strong></small></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-10 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-2 col-3"><small><strong>1 stars</strong></small></div>
							</div>
							<!-- /row -->
						</div>
					</div>
					<!-- /row -->
				</div>

				<div class="reviews-container">

					<div class="review-box clearfix">
						<figure class="rev-thumb"><img src="img/avatar1.jpg" alt="">
						</figure>
						<div class="rev-content">
							<div class="rating">
								<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
							</div>
							<div class="rev-info">
								Admin – April 03, 2016:
							</div>
							<div class="rev-text">
								<p>
									Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
								</p>
							</div>
						</div>
					</div>
					<!-- /review-box -->
					<div class="review-box clearfix">
						<figure class="rev-thumb"><img src="img/avatar2.jpg" alt="">
						</figure>
						<div class="rev-content">
							<div class="rating">
								<i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
							</div>
							<div class="rev-info">
								Ahsan – April 01, 2016:
							</div>
							<div class="rev-text">
								<p>
									Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
								</p>
							</div>
						</div>
					</div>
					<!-- /review-box -->
					<div class="review-box clearfix">
						<figure class="rev-thumb"><img src="img/avatar3.jpg" alt="">
						</figure>
						<div class="rev-content">
							<div class="rating">
								<i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
							</div>
							<div class="rev-info">
								Sara – March 31, 2016:
							</div>
							<div class="rev-text">
								<p>
									Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
								</p>
							</div>
						</div>
					</div>
					<!-- /review-box -->
				</div>
				<!-- /review-container -->
			</section>
				<!--/row-->
			</div>
			<!--/container-->
		</div>
		<!--/bg_color_1-->



@endsection

@section('javascript')
@endsection