
@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')

<div class="container margin_60">
	<div class="row justify-content-center">
		<div class="col-md-5">
			<div id="confirm">
				<div class="icon icon--order-success svg add_bottom_15">
					<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
						<g fill="none" stroke="#8EC343" stroke-width="2">
							<circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
							<path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
						</g>
					</svg>
				</div>
				<h2>Booking Confirmed!</h2>
				<p>Thank you for completing our extended booking form. That's it - we now have all of the details. As always, if you have any other questions, please feel free to contact us directly on 1300 30 49 40. We're as excited as you are - see you at your celebration!</p>
				<p>We will get in touch with you shortly.</p>

				<div class="text-center">
					<a href="{{ route('publicPageCheckout') }}" class="btn btn-primary">Chekout this booking</a>
				</div>
			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->


@endsection

@section('javascript')
@endsection