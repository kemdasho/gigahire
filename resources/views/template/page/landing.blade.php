@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')

<section class="hero_single version_2">
    <div class="wrapper">
        <div class="container">
            <h3>We Provide Amazing DJ<br>Services In Sydney</h3>
            <p>We’re Sydney premium DJ supplier and we’ve served customers for over 6000 weddings, engagements, corporate functions, parties and school formals to date!</p>

            <div class="row">
                <div class="col-md-3 offset-md-2 text-right pt-3">
                    <p>I need a Service for</p>
                </div>
                <div class="col-md-5">
                    <form method="post" action="grid-listings-filterscol.html">
                        <div class="row no-gutters custom-search-input-2">
                            <div class="col-lg-8">
                                <select class="wide">
                                    <option>All Categories</option> 
                                    <option>A Wedding</option>
                                    <option>An Engagement</option>
                                    <option>A Corporate Function</option>
                                    <option>A Party</option>
                                    <option>A School Formal</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <input type="submit" value="Search">
                            </div>
                        </div>
                        <!-- /row -->
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!-- /hero_single -->

<div class="container-fluid margin_80_55">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Who We’ve Served</h2>
        <p>We are a trusted brand within the industry. Here are some customers we’ve served.</p>
    </div>
    <div id="reccomended" class="owl-carousel owl-theme">
        <div class="item">
            <div class="strip grid">
                <figure>
                    <a href="#"><img src="{{ URL::asset('assets/img/location_1.jpg') }}" class="img-fluid" alt="" width="400" height="266"></a>
                    <small>Restaurant</small>
                </figure>
                <div class="wrapper">
                    <h3><a href="#">Da Alfredo</a></h3>
                    <p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
                </div>
                <ul>
                    <li>
                        <div class="cat_star">
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                        </div>
                    </li>
                    <li><div class="score"><strong>8.9</strong></div></li>
                </ul>
            </div>
        </div>
        <!-- /item -->

        <div class="item">
            <div class="strip grid">
                <figure>
                    <a href="#"><img src="{{ URL::asset('assets/img/location_1.jpg') }}" class="img-fluid" alt="" width="400" height="266"></a>
                    <small>Restaurant</small>
                </figure>
                <div class="wrapper">
                    <h3><a href="#">Da Alfredo</a></h3>
                    <p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
                </div>
                <ul>
                    <li>
                        <div class="cat_star">
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                        </div>
                    </li>
                    <li><div class="score"><strong>8.9</strong></div></li>
                </ul>
            </div>
        </div>
        <!-- /item -->

        <div class="item">
            <div class="strip grid">
                <figure>
                    <a href="#"><img src="{{ URL::asset('assets/img/location_1.jpg') }}" class="img-fluid" alt="" width="400" height="266"></a>
                    <small>Restaurant</small>
                </figure>
                <div class="wrapper">
                    <h3><a href="#">Da Alfredo</a></h3>
                    <p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
                </div>
                <ul>
                    <li>
                        <div class="cat_star">
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                        </div>
                    </li>
                    <li><div class="score"><strong>8.9</strong></div></li>
                </ul>
            </div>
        </div>
        <!-- /item -->

        <div class="item">
            <div class="strip grid">
                <figure>
                    <a href="#"><img src="{{ URL::asset('assets/img/location_1.jpg') }}" class="img-fluid" alt="" width="400" height="266"></a>
                    <small>Restaurant</small>
                </figure>
                <div class="wrapper">
                    <h3><a href="#">Da Alfredo</a></h3>
                    <p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
                </div>
                <ul>
                    <li>
                        <div class="cat_star">
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                            <i class="icon_star"></i>
                        </div>
                    </li>
                    <li><div class="score"><strong>8.9</strong></div></li>
                </ul>
            </div>
        </div>
        <!-- /item -->
    </div>
    <!-- /carousel -->
</div>
<!-- /container -->

<div class="container margin_80_55">
    <div class="row">
        <div class="offset-xl-2 col-xl-8 col-md-12">
            <div class="main_title_2">
        <span><em></em></span>
        <h2>Our Live Feeds</h2>
        <p class="mt-3">We absolutely love what we do. Each celebration that we’re involved with, we strive to provide a 5-star experience – the Star DJ experience! Below is a live feed of our average ratings on Facebook, Google and Product Review from events we’ve provided DJs for in Sydney & Canberra.</p>
    </div>
        </div>
    </div>
    
    <div class="row ">
        <div class="col-lg-3 col-sm-6">
            <a href="#" class="grid_item small">
                <figure>
                    <img src="{{ URL::asset('assets/img/hotel_1.jpg') }}" alt="">
                    <div class="info">
                        <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                        <h3>Facebook</h3>
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-lg-3 col-sm-6">
            <a href="#" class="grid_item small">
                <figure>
                    <img src="{{ URL::asset('assets/img/hotel_1.jpg') }}" alt="">
                    <div class="info">
                        <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                        <h3>Google</h3>
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-lg-3 col-sm-6">
            <a href="#" class="grid_item small">
                <figure>
                    <img src="{{ URL::asset('assets/img/hotel_1.jpg') }}" alt="">
                    <div class="info">
                        <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                        <h3>Facebook</h3>
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-lg-3 col-sm-6">
            <a href="#" class="grid_item small">
                <figure>
                    <img src="{{ URL::asset('assets/img/hotel_1.jpg') }}" alt="">
                    <div class="info">
                        <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                        <h3>Google</h3>
                    </div>
                </figure>
            </a>
        </div>

    </div>
</div>

<div class="container margin_80_55">
    <div class="main_title_2">
        <span><em></em></span>
        <h2>Why Us?</h2>
        <p class="mt-3">We’ve summarised what we’re most proud of</p>
    </div>
    <div class="row">
        <div class="col-md-6">
            <ul class="list_ok">
                <li>Consistent 5-star independent reviews across platforms like Facebook, Google and Product Review.</li>
                <li>Our DJs hold relevant licenses and insurances.</li>
                <li>Our packages include all required equipment at no extra cost.</li>
                <li>We regularly play at Sydney & Canberra’s most prestigious venues like Doltone House, Le Montage, and Oatlands House.</li>
                <li>Our team is regularly trained and upskilled.</li>
                <li>We have backup DJs and equipment for your peace of mind.</li>
            </ul>
        </div>
        <div class="col-md-6 text-center">
            <img src="{{ URL::asset('assets/img/blog-1.jpg') }}" alt="" class="img-fluid add_bottom_45">
        </div>
    </div>
</div>
<!-- /container -->

<div class="bg_color_1">
    <div class="container margin_80_55">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>Gigahire Hire in Sydney</h2>
            <p class="mt-3">Star DJ Hire is Sydney’s premium DJ supplier. We provide DJ services throughout Sydney for all parties, functions, and celebrations. From private function and birthday party DJ hire, through to corporate and wedding DJ hire.</p>
            <p>Our packages offer a premium DJ service and will leave you and your guests with a memory to share forever. Our DJs have respectable experience in the industry and provide the best music from a wide range of genres. Our packages include all of the essentials with no extra or hidden costs! We guarantee our service.</p>
        </div>
        <div class="row">
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="pe-7s-medal"></i>
                        <h3>+ 1000 Customers</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="pe-7s-help2"></i>
                        <h3>H24 Support</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris. </p>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="pe-7s-culture"></i>
                        <h3>+ 5575 Locations</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-2"></div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="pe-7s-headphones"></i>
                        <h3>Help Direct Line</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris. </p>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="pe-7s-credit"></i>
                        <h3>Secure Payments</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>
                    </a>
                </div>
            </div>
        <!-- /row -->
    </div>
    <!-- /container --> 
</div>
<!-- /bg_color_1 -->

<div class="call_section image_bg">
    <div class="wrapper">
        <div class="container margin_80_55">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Hire Us</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="box_how wow">
                        <i class="pe-7s-search"></i>
                        <h3>Search Locations</h3>
                        <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                        <span></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box_how">
                        <i class="pe-7s-info"></i>
                        <h3>View Location Info</h3>
                        <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                        <span></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box_how">
                        <i class="pe-7s-like2"></i>
                        <h3>Book, Reach or Call</h3>
                        <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                    </div>
                </div>
            </div>
            <!-- /row -->
            <p class="text-center add_top_30 wow bounceIn" data-wow-delay="0.5"><a href="{{ route('publicPagePricing') }}" class="btn_1 rounded">Enquire Now</a></p>
        </div>
    </div>
    <!-- /wrapper -->
</div>
<!--/call_section-->



@endsection

@section('javascript')
@endsection