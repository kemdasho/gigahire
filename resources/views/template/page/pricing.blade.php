@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')

<div class="container margin_60_35">

	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6">
			<div class="strip grid">
				<figure>
					<a href="detail-restaurant.html"><img src="img/location_1.jpg" class="img-fluid" alt=""><div class="read_more"><span>Read more</span></div></a>
					<small>Restaurant</small>
				</figure>
				<div class="wrapper">
					<h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
					<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
				</div>
				<ul>
					<li>From $650 + GST for entire event</li>
					<li><div class="score"><span>Superb<em>350 Reviews</em></span><strong>8.9</strong></div></li>
				</ul>
			</div>
		</div>

		<div class="col-xl-6 col-lg-6 col-md-6">
			<div class="strip grid">
				<figure>
					<a href="detail-restaurant.html"><img src="img/location_1.jpg" class="img-fluid" alt=""><div class="read_more"><span>Read more</span></div></a>
					<small>Restaurant</small>
				</figure>
				<div class="wrapper">
					<h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
					<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
				</div>
				<ul>
					<li>From $650 + GST for entire event</li>
					<li><div class="score"><span>Superb<em>350 Reviews</em></span><strong>8.9</strong></div></li>
				</ul>
			</div>
		</div>

		<div class="col-xl-6 col-lg-6 col-md-6">
			<div class="strip grid">
				<figure>
					<a href="detail-restaurant.html"><img src="img/location_1.jpg" class="img-fluid" alt=""><div class="read_more"><span>Read more</span></div></a>
					<small>Restaurant</small>
				</figure>
				<div class="wrapper">
					<h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
					<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
				</div>
				<ul>
					<li>From $650 + GST for entire event</li>
					<li><div class="score"><span>Superb<em>350 Reviews</em></span><strong>8.9</strong></div></li>
				</ul>
			</div>
		</div>

		<div class="col-xl-6 col-lg-6 col-md-6">
			<div class="strip grid">
				<figure>
					<a href="detail-restaurant.html"><img src="img/location_1.jpg" class="img-fluid" alt=""><div class="read_more"><span>Read more</span></div></a>
					<small>Restaurant</small>
				</figure>
				<div class="wrapper">
					<h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
					<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
				</div>
				<ul>
					<li>From $650 + GST for entire event</li>
					<li><div class="score"><span>Superb<em>350 Reviews</em></span><strong>8.9</strong></div></li>
				</ul>
			</div>
		</div>

	</div>
	<!-- /row -->


</div>
<!-- /container -->

@endsection

@section('javascript')
@endsection