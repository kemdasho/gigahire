
@extends('template.layouts.master')

@section('stylesheet')
@endsection

@section('content')

<div class="container margin_60">
	<div class="row justify-content-center">
		<div class="col-md-5">
			<div id="confirm">
				<div class="icon icon--order-success svg add_bottom_15">
					<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
						<g fill="none" stroke="#8EC343" stroke-width="2">
							<circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
							<path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
						</g>
					</svg>
				</div>
				<h2>Payment Confirmed!</h2>
				<p>Thank you for completing our extended booking form. That's it - we now have all of the details. As always, if you have any other questions, please feel free to contact us directly on 1300 30 49 40. We're as excited as you are - see you at your celebration!</p>
				<p>We will get in touch with you shortly.</p>

				<div class="text-center">
					<a href="{{ route('publicPageBookManager') }}" class="btn btn-primary">Booking manager</a>
				</div>
			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->

<div class="bg_color_1">
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-lg-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-help2"></i>
							<h4>Need Help? Contact us</h4>
							<p>Cum appareat maiestatis interpretaris et, et sit.</p>
						</a>
					</div>
					<div class="col-lg-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-wallet"></i>
							<h4>Payments</h4>
							<p>Qui ea nemore eruditi, magna prima possit eu mei.</p>
						</a>
					</div>
					<div class="col-lg-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-note2"></i>
							<h4>Cancel Policy</h4>
							<p>Hinc vituperata sed ut, pro laudem nonumes ex.</p>
						</a>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
@endsection

@section('javascript')
@endsection