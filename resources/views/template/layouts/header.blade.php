@isset ($bodyHeader)
    <header class="header_in is_sticky menu_fixed">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div id="logo">
                        <a href="index.html">
                            <img src="{{ URL::asset('assets/img/Giga_black.png') }}" height="55" alt="" class="logo_sticky">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-12">
                    @include('template.layouts.menu')
                </div>
            </div>
            <!-- /row -->
        </div>
    </header>

    <div class="sub_header_in sticky_header">
        <div class="container">
            <h1>{{ $bodyHeader['name'] }}</h1>
        </div>
        <!-- /container -->
    </div>
@else
    <header class="header menu_fixed">
        <div id="logo">
            <a href="home.html" title="Sparker - Directory and listings template">

                <img src="{{ URL::asset('assets/img/Giga_whitefull.png') }}" height="55" alt="" class="logo_normal">
                <img src="{{ URL::asset('assets/img/Giga_black.png') }}" height="55" alt="" class="logo_sticky">
            </a>
        </div>
        @include('template.layouts.menu')
    </header>
@endisset