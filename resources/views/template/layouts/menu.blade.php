<ul id="top_menu">
        <li><a href="{{ route('publicPagePricing') }}" class="btn_add">Enquire now</a></li>
    </ul>
    <!-- /top_menu -->
    <a href="#menu" class="btn_mobile">
        <div class="hamburger hamburger--spin" id="hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
    </a>
    <nav id="menu" class="main-menu">
        <ul>
            <li><span><a href="{{ route('publicPageLanding') }}">Home</a></span>
            </li>
            <li><span><a href="#0">Events</a></span>
                <ul>
                    <li><a href="{{ route('publicPageSingleEvent', ['event' => 'weddings']) }}">Weddings</a></li>
                </ul>
            </li>
            <li><span><a href="{{ route('publicPageTeam') }}">Team</a></span></li>
            <li><span><a href="{{ route('publicPageReview') }}">Reviews</a></span></li>
            <li><span><a href="{{ route('publicPagePricing') }}">Pricing</a></span></li>
            <li><span><a href="{{ route('publicPageBlog') }}">Blog</a></span></li>
            <li><span><a href="{{ route('publicPageContact') }}">Contact us</a></span></li>
        </ul>
    </nav>