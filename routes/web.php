<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@publicPageLanding')->name('publicPageLanding');

Route::get('/event', 'PublicController@publicPageEvent')->name('publicPageEvent');

Route::get('/event/{event}', 'PublicController@publicPageSingleEvent')->name('publicPageSingleEvent');

Route::post('/book/event', 'PublicController@publicActionBookEvent')->name('publicActionBookEvent');

Route::get('/book/success', 'PublicController@publicPageBookSuccess')->name('publicPageBookSuccess');

Route::get('/book/checkout', 'PublicController@publicPageCheckout')->name('publicPageCheckout');

// Route::post('/book/checkout/payment', 'PublicController@publicPageCheckoutPayment')->name('publicPageCheckoutPayment');

Route::get('/book/checkout/success', 'PublicController@publicPageCheckoutSuccess')->name('publicPageCheckoutSuccess');

Route::get('/book/manager', 'PublicController@publicPageBookManager')->name('publicPageBookManager');

Route::get('/book/invoice', 'PublicController@publicPageBookInvoice')->name('publicPageBookInvoice');

Route::post('/leave/rating', 'PublicController@publicActionLeaveRating')->name('publicActionLeaveRating');

// page section

Route::get('/page/team', 'PublicController@publicPageTeam')->name('publicPageTeam');

Route::get('/page/review', 'PublicController@publicPageReview')->name('publicPageReview');

Route::get('/page/pricing', 'PublicController@publicPagePricing')->name('publicPagePricing');

Route::get('/page/contact', 'PublicController@publicPageContact')->name('publicPageContact');

Route::get('/blog', 'PublicController@publicPageBlog')->name('publicPageBlog');

Route::get('/blog/{blog}', 'PublicController@publicPageSingleBlog')->name('publicPageSingleBlog');